Data set is the Kaggle "House Prices: Advanced Regression Techniques" problem,in which the aim is to predict the prices of the houses based on the available data.

Techniques used are:
Linear regression
Lasso regression
Ridge regression
